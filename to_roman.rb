def digits(n)
    Math.log10(n).floor.downto(0).map { |i| (n / 10**i) % 10 }
end

module Rome
  NUMERALS = {
    "I" =>  1,
    "V" =>  5,
    "X" =>  10,
    "L" =>  50,
    "C" =>  100,
    "D" =>  500,
    "M" =>  1000,
  }

  NUMERAL_ARRAY = NUMERALS.to_a

  def self.to(number)
    reduce(digits(number))
  end

  private

  def self.reduce(digits = [])
    digits.reduce(["", digits.size - 1]) do |(string, power), digit|
      amount = digit * (10 ** power)
      if digit == 0 # nothing
      elsif NUMERALS.key(amount)
        string += NUMERALS.key(amount)
      else
        closest_numeral_index = NUMERAL_ARRAY.rindex {|x| x.last <= amount }
        numeral = NUMERAL_ARRAY[closest_numeral_index].first
        next_numeral = NUMERAL_ARRAY[closest_numeral_index + 1].first

        if digit == 4
          string += numeral + next_numeral
        elsif digit == 9
          previous_numeral = NUMERAL_ARRAY[closest_numeral_index - 1].first
          string += previous_numeral + next_numeral
        elsif digit < 5
          string += numeral * digit
        else
          repeating_numeral, repeating_value = NUMERAL_ARRAY[closest_numeral_index - 1]
          number = NUMERAL_ARRAY[closest_numeral_index].last
          sum = amount - number
          times_to_repeat = sum / repeating_value
          string += numeral + repeating_numeral * times_to_repeat
        end
      end
      [string, power - 1]
    end.first
  end
end

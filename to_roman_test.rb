require "minitest/autorun"
require "./to_roman"

describe "Rome::to" do
  it "handles zeros" do
    assert_equal "CCCXX", Rome::to(320)
  end

  it "handles large numbers of repeating characters" do
    assert_equal "MCMXCIX", Rome::to(1999)
  end

  it "handles large numbers of repeating characters" do
    assert_equal "MCMXCIV", Rome::to(1994)
  end

  it "handles large numbers of repeating characters" do
    assert_equal "MCMXCIII", Rome::to(1993)
  end

  it "handles large numbers without repeating characters" do
    assert_equal Rome::to(1990), Rome::to(1990)
  end

  it "handles odd characters" do
    assert_equal "IX", Rome::to(9)
  end

  it "handles numbers that fit exactly into roman numerals" do
    assert_equal "C", Rome::to(100)
  end

  it "handles 4" do
    assert_equal "IV", Rome::to(4)
  end

  it "handles 40" do
    assert_equal "XL", Rome::to(40)
  end

  it "handles 6" do
    assert_equal "VI", Rome::to(6)
  end

  it "handles 60" do
    assert_equal "LX", Rome::to(60)
  end

  it "handles 766" do
    assert_equal "DCCLXVI", Rome::to(766)
  end

  it "handles 499" do
    assert_equal "CDXCIX", Rome::to(499)
  end
end
